#pragma once
#include <string>
using namespace std;

class Employee
{
protected:
	int id; 
	string name; 
	int time = 100; // ������������ �����
	int salary; 
	int budget; 
	int numberEmp; // ���-�� ����������
	string position; // ���
public:
	Employee() {};

	virtual void Pay() {}
	int GetTime() const 
	{ 
		return time;
	}
	int GetPayment() const 
	{
		return salary;
	}
	int getID() 
	{ 
		return id;
	}
	string GetName() const
	{ 
		return name;
	}
	string GetPosition() const 
	{ 
		return position; 
	}
	void SetPosition(string position) 
	{ 
		this->position = position; 
	}
};