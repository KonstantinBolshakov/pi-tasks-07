#pragma once
#include "employee.h"
#include "interface.h"


class Personal : public Employee, public Worktime
{
protected:
	int base;
public:
	Personal() 
	{};

	Personal(int id, string name, int base)
	{
		this->id = id;
		this->name = name;
		this->base = base;
	}

	int PayTime(int time, int base) { return time * base; }
};

class Cleaner : public Personal
{
public:
	Cleaner(int id, string name, int base)
	{
		this->id = id;
		this->name = name;
		this->base = base;
		salary = 0;
	}

	void Pay()
	{
		salary = PayTime(time, base);
	}

};

class Driver : public Personal
{
public:
	Driver(int id, string name, int base)
	{
		this->id = id;
		this->name = name;
		salary = 0;
		this->base = base;
	}

	void Pay()
	{
		salary = PayTime(time, base);
	}
};